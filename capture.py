import time

__author__ = 'Atomu'

import arff


data_path = "D:\Pystudio\CN-proj\dataset1\\"
file_names = [
    "entry01.weka.allclass.arff",
    "entry02.weka.allclass.arff",
    "entry03.weka.allclass.arff",
    "entry04.weka.allclass.arff",
    "entry05.weka.allclass.arff",
    "entry06.weka.allclass.arff",
    "entry07.weka.allclass.arff",
    "entry08.weka.allclass.arff",
    "entry09.weka.allclass.arff",
    "entry10.weka.allclass.arff",
    "entry12.weka.allclass.arff"
]
yn_cols = [64, 65, 66, 67, 70, 71]
none_cols = [68, 69, 72, 73, 98, 99, 100, 101, 109, 110, 111, 208, 224, 225, 226, 227, 234, 235, 236, 237, 242, 243,
             244, 245, 246, 247]


if __name__ == '__main__':
    for file_name in file_names:
        data = arff.load(open(data_path+file_name, 'r'))
        myentry = open(data_path+'my'+file_name, 'a')
        l = len(data['data'])
        print l
        for i in range(l-1, 3, -4):
            del data['data'][i-1]
            del data['data'][i-2]
            del data['data'][i-3]
        print len(data['data'])

        arff.dump(data, myentry)
