import time

import arff
from sklearn.decomposition import PCA
from sklearn import svm
import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

__author__ = 'Atomu'

data_path = "D:\Pystudio\CN-proj\dataset1\\"
file_names = [
    "entry01.weka.allclass.arff",
    "entry02.weka.allclass.arff",
    "entry03.weka.allclass.arff",
    "entry04.weka.allclass.arff",
    "entry05.weka.allclass.arff",
    "entry06.weka.allclass.arff",
    "entry07.weka.allclass.arff",
    "entry08.weka.allclass.arff",
    "entry09.weka.allclass.arff",
    "entry10.weka.allclass.arff",
    "entry12.weka.allclass.arff"
]
yn_cols = [64, 65, 66, 67, 70, 71]
none_cols = [68, 69, 72, 73, 98, 99, 100, 101, 109, 110, 111, 208, 224, 225, 226, 227, 234, 235, 236, 237, 242, 243,
             244, 245, 246, 247]


def read_arff():
    data = arff.load(open(data_path + file_names[10], "r"))
    print file_names[10]
    print data.keys()
    print data["attributes"]
    print data["relation"]
    print data["description"]
    print len(data["data"]), data["data"][0]
    return numpy.array(data["data"])


if __name__ == "__main__":
    data = read_arff()

    start = time.time()
    flag = True
    labels = []
    # print data[39463][-1] == u'ATTACK'
    for i in range(len(data)):
        if data[i][-1] == u"ATTACK":
            data[i][-1] = 1
            print "attack", i
        else:
            data[i][-1] = 0
        labels.append(data[i][-1])
        for j in yn_cols:
            if data[i][j] is unicode("Y"):
                data[i][j] = 1
            else:
                data[i][j] = 0
        for j in none_cols:
            if data[i][j] is None:
                data[i][j] = 0

    print time.time() - start
    print data[0]
    print data.shape

    data = data[:, :-1]
    labels = labels[:]

    print len(data), len(labels)
    start = time.time()
    # print "pca start at: ", start
    # pca = PCA(n_components=3)
    # data = pca.fit_transform(data)
    # end = time.time()
    # print "pca finish at: ", end

    # print "svc start at: ", start
    # clf = svm.SVC()
    # clf.fit(data, labels)
    # end = time.time()
    # print "svc finish at: ", end
    # print end - start

    # print "save"
    from sklearn.externals import joblib
    # joblib.dump(clf, "clf.pkl")
    # print "save finish"

    clf = joblib.load("clf.pkl")
    predict = []
    start = time.time()
    print "test start at: ", start
    for item in data:
        predict.append(clf.predict(item))
    end = time.time()
    print "test finish at: ", end
    print end - start
    count = 0

    for i in range(len(predict)):
        if predict[i] != labels[i]:
            count += 1

    print "clf predict ", count, len(data)

    #
    # clf2 = joblib.load("clf.pkl")
    # predict = []
    # for item in data:
    #     predict.append(clf2.predict(item))
    # count = 0
    # for i in range(len(predict)):
    #     if predict[i] != labels[i]:
    #         count += 1
    #
    # print "clf2 predict ", count
